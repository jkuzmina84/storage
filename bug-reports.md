# Составление баг-репортов
Интуитивное тестирование формы авторизации. Требования ПО:  
- username: admin  
- password: admin  
- В форме авторизации выводится сообщение Logged In Successfully при вводе корректных авторизационных данных;  
- В форме авторизации выводится сообщение Enter Both Username And Password, если хотя бы одно из полей не заполнено;  
- В форме авторизации выводится сообщение Username Not Found, если введено любое другое значение username;  
- В форме авторизации выводится сообщение Incorrect password, если введён верный username и неверный пароль;  
- Все сообщения выводятся над кнопкой Log In;  
- Форма авторизации предлагает выбрать один из четырёх языков интерфейса: английский, французский, итальянский, немецкий;  
- Выбранный язык не сбрасывается после попытки авторизации.
****

0. Тестирование полей - имеется форма для ввода, вводятся данные, есть кнопка выхода - нет бага
1. Просмотр введенного пароля - баг
2. admin/admin  - баг
2. admin/neadmin - incorrect password - баг
3. neadmin/admin - username not found - баг
4. admin/empty - enter both password - Нет бага  
5. Сообщение смещено - баг
6. Выбор языка интерфейса - языки меняются - нет бага
7. Сохранения выбранного языка после авторизации -баг

### Баг №1 Корректное имя пользователя при авторизации не найдено (тип бага Функциональный/ Blocker)  
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/  
**Шаги:**
1. В поле Username ввести admin. 
2. В поле Password ввести admin. 
3. Нажать кнопку "Log in"   

**Ожидаемый результат**: Пользователь авторизован, над кнопкой "Log in" появилось сообщение "Log In Successfull"   
**Фактический результат**: Пользователь не найден, сообщение "Username Not FoundLog In Successfull" не соответсвует критерию ввода, надпись смещена в левый верхний угол  
**Окружение**: Windows 10, Chrome  
**Приоритет**: Hight


### Баг №2 Неверное сообщение об ошибке при авторизации (тип бага Функциональный / Blocker)  
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/  
**Шаги:**
1. В поле Username ввести admin
2. В поле Password ввести neadmin
3. Нажать кнопку "Log in"  

**Ожидаемый результат** - Пользователь не авторизован, над кнопкой "Log in" появилось ссобщение "Incorrect password"  
**Фактический результат** - выводится сообщение "Username Not FoundLog In Successfull", не соответсвует критерию заполнения полей  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Hight

### Баг №3 Неверное сообщение об ошибке при авторизации (тип бага Функциональный / Blocker)  
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/  
**Шаги:**
1. В поле Username ввести neadmin
2. В поле Password ввести admin 
3. Нажать кнопку "Log in"  

**Ожидаемый результат** - Пользователь не авторизован. Над кнопкой "Log in" выводится сообщение "Username Not Found"  
**Фактический результат** - выводится сообщение "Username Not FoundLog In Successfull", не соответсвует критерию ввода, надпись смещена в левый угол   
**Окружение** - Windows 10, Chrome  
**Приоритет** - Hight  

### Баг №4 - Неверное расположение сообщения (тип бага Визуальный / Trivial) ### 
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/  
**Шаги:**
1. В поле Username ввести имя пользователя
2. В поле Password ввести пароль
3. Нажать кнопку "Log in"  

**Ожидаемый результат** - любое сообщение выведено над кнопкой "Log in"  
**Фактический результат** - Сообщение выведено в левый верхний угол экрана  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Low

### Баг №5 Не сохраняется выбранный итальянский язык интерфейса после авторизации (тип бага Логический / Minor)
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/. По умолчанию интерфейс на английском языке.  

**Шаги:**
1. Выбрть итальянский язык интерфейса.
2. В поле Username ввести admin
3. В поле Password ввести admin  
4. Нажать кнопку "Log in"
 
**Ожидаемый результат** После попытки авторизации язык интерфейса формы сохранился итальянским.  
**Фактический результат** -  После попытки авторизации язык интерфейса формы сменился на английский.  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Low

### Баг №6 Не сохраняется выбранный французский язык интерфейса после авторизации (тип бага Логический / Minor)  
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/  
**Шаги:**
1. Выбрть французский язык интерфейса.
2. В поле Username ввести admin
3. В поле Password ввести admin  
4. Нажать кнопку "Log in"
 
**Ожидаемый результат** После попытки авторизации язык интерфейса формы сохранился французским.  
**Фактический результат** -  После попытки авторизации язык интерфейса формы сменился на английский.  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Low

### Баг №7 Не сохраняется выбранный немецкий язык интерфейса после авторизации (тип бага Логический / Minor)  
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/  
**Шаги:**
1. Выбрать немецкий язык интерфейса.
2. В поле Username ввести admin
3. В поле Password ввести admin  
4. Нажать кнопку "Log in"
 
**Ожидаемый результат** После попытки авторизации язык интерфейса формы сохранился немецким.  
**Фактический результат** -  После попытки авторизации язык интерфейса формы сменился на английский.  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Low

### Баг №8 Не выводится сообщение о незаполненных полях (тип бага Логический / Blocker)  
**Шаги:**
1. Поле Username оставить пустым
2. Поле Password оставить пустыми. 
3. Нажать кнопку "Log in"  

**Ожидаемый результат** - При попытке авторизаци выводится сообщение о необходимости заполнить пустые поля.  
**Фактический результат** - При нажатии кнопки Login не выводится никакого предупреждения о незаполненных полях, ничего не происходит и не меняется в интерфейсе.  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Low

### Баг №9 Сообщения не разделены между собой (Тип бага Визуальный / Minor) ###

**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/. Заполнены поля Username и Password  
**Шаги:**
1. Нажать кнопку "Log in"   

**Ожидаемый результат** - выведено корректное сообщение в соответствии с критериями ввода  
**Фактический результат** - сообщение "Username Not FoundLog In Successfull" состоит из двух предложений, не разделено между собой спец.символом  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Medium

### Баг №10 Нет возможности просматривать введение пароля (тип бага Визуальный/Major)  
**Предусловие** - Открыта страница авторизации http://testingchallenges.thetestingmap.org/. Заполнено поле Username   
**Шаги:**  
1. заполнить поле Password   

**Ожидаемый результат** - при нажатии на кнопку просмотра пароля "глаз" в поле Password меняются символы "точек" на введенные значения  
**Фактический результат** - кнопка "глаз" отсутствует в форме авторизации, нет возможности просмотра введения пароля.  
**Окружение** - Windows 10, Chrome  
**Приоритет** - Medium


